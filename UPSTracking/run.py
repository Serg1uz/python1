import requests

session = requests.session()

session.headers.update({'Accept': 'application/json, text/plain, */*'})
session.headers.update({'Accept-Encoding': 'gzip, deflate, br'})
session.headers.update({'Content-Type': 'application/json'})
session.headers.update({'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36'})

# Auth
url = 'https://www.ups.com/lasso/login?userID=serg1uz&password=130383Qaz'
response = session.post(url=url)
# # Temp
url = 'https://www.ups.com/track?loc=en_UA&requester=WT/'
response = session.get(url=url)
token = response.cookies.get(name='X-XSRF-TOKEN-ST')
session.headers.update({'X-XSRF-TOKEN':token})
for h in session.headers:
    print(f'{h} - {session.headers[h]}')

for c in session.cookies:
    print(c)

url = 'https://www.ups.com/track/api/Track/GetStatus?loc=en_US'
data = {"Locale": "en_US", "TrackingNumber": ["1Z1358700363109534"]}
response = session.post(url=url, data=data)
print(response.request.headers)
