from allauth.account.signals import user_signed_up
from django.db import models
# from .signals import retrieve_social_data
# Create your models here.

from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver


class AccountUser(AbstractUser):
    """ Расширение базовой модели user"""
    external_id = models.CharField('External_id', max_length=100, blank=True, null=True)



@receiver(user_signed_up)
def retrieve_social_data(request, user, **kwargs):
    print(f'Request {request} from user {user}')
