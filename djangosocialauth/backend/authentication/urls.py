from django.urls import path
from . import views


urlpatterns = [
    path('google/', views.GoogleLogin.as_view(), name='google_login'),
    path('bitbucket/', views.BitbucketLogin.as_view(), name='bitbucket_login')
]