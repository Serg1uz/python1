from allauth.socialaccount.providers.google.views import GoogleOAuth2Adapter
from allauth.socialaccount.providers.bitbucket_oauth2.views import BitbucketOAuth2Adapter
from rest_auth.registration.views import SocialLoginView


class GoogleLogin(SocialLoginView):
    adapter_class = GoogleOAuth2Adapter


class BitbucketLogin(SocialLoginView):
    adapter_class = BitbucketOAuth2Adapter