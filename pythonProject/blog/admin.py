from django.contrib import admin
from .models import Post


# class PostAdmin(admin.ModelAdmin):
#     list_display = ('__all__')
#     list_filter = ('status')

admin.site.register(Post)