import time

from selenium import  webdriver


base_url = 'https://handbook2.megadepotllc.com'
# base_url = 'https://handbook2-hotfix.megadepotllc.com:20443'

login_url = '/auth/login'
terms_url = '/manufacturer/terms?statuses=account-set-up&sort=-accounting.paymentDiscountTariff'
term_url = '/manufacturer/term/'

lusername = 'schernov@megadepotllc.com'
lpassw = 'Terranova13'
# lpassw = 'master'

driver = webdriver.Chrome()
driver.get(base_url+login_url)

time.sleep(2)
#login
driver.find_element_by_id('loginForm-username').clear()
driver.find_element_by_id('loginForm-username').send_keys(lusername)
driver.find_element_by_id('loginForm-password').clear()
driver.find_element_by_id('loginForm-password').send_keys(lpassw)
driver.find_element_by_name('login-button').click()
time.sleep(1)

#go to need terms
driver.get(base_url+terms_url)
time.sleep(1)

#get table data
urls = []
table = driver.find_elements_by_tag_name('tbody')[0]
trs = table.find_elements_by_tag_name('tr')
for tr in trs:
    id = tr.find_elements_by_tag_name('td')[1].text
    name = tr.find_element_by_class_name('name')
    link = name.find_element_by_tag_name('a')
    url = link.get_attribute('href')
    discount = tr.find_element_by_class_name('accounting-paymentDiscountTariff').text
    if discount[0:2] == '0%':
        print(discount[0:2])
        break
    urls.append(url[:-14])

print(urls)
id = 0
for url in urls:
    #re save term
    # print(f'{id} - {url[:-14]} - {discount}')
    id += 1
    driver.get(url+'/update')
    time.sleep(0.5)
    driver.find_element_by_xpath('//*[@id="main"]/header/p/button').click()
    time.sleep(2)
    print(f"{id} - {url.split('/')[-1]} ")
    # break
# print(table.text)

