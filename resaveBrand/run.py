import requests
from bs4 import BeautifulSoup as BS


# import logging

# base_url = 'https://handbook2.megadepotllc.com/'
base_url = 'https://handbook2-hotfix.megadepotllc.com:20443/'

login_url = '/auth/login'
terms_url = '/manufacturer/terms?statuses=account-set-up&sort=-accounting.paymentDiscountTariff'
term_url = '/manufacturer/term/'

lusername = 'schernov@megadepotllc.com'
# lpassw = 'terranova13'
lpassw = 'master'

session = None
response = None


def get_csrf():
    global response
    if response is None:
        return False
    soup = BS(response.text, 'html.parser')
    metas = soup.find_all('meta')

    for meta in metas:
        if meta.get('name') == 'csrf-token': return meta['content']
    return False


def login():
    global response
    global session
    if session is None or response is None:
        return False
    csrf = get_csrf()
    if not csrf:
        return False
    data = {'_csrf': csrf, 'LoginForm[username]': lusername, 'LoginForm[password]': lpassw,
            'login-button': ''}
    response = session.post(base_url + login_url, data=data)

    return response.status_code == 200 or response.status_code == 302

def re_save(uri: str):
    global response
    global session

    try:
        url = base_url + term_url +  uri + '/update'
        response = session.get(url, verify=False)
        csrf = get_csrf()

