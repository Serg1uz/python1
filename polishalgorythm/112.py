function infixToPostfix(expr) {
    var i = 0,
        nextToken = function() {
            while (i < expr.length && expr[i] == ' ') i++;
            if (i == expr.length) return '';
            var b = '';
            while (i < expr.length && expr[i] != ' ' && expr[i] != '(' && expr[i] != ')' && !isOperator(expr[i])) b += expr[i++];
            if (b != '') return b;
            return expr[i++];
        };
    var S = [],
    O = [],
        tok;
    while ((tok = nextToken()) != '') {
        if (tok == '(') S.push(tok);
        else if (tok == ')') {
            while (S.length > 0 && S[S.length - 1] != '(') O.push(S.pop());
            if (S.length == 0) return 'Mismatched parenthesis.';
            S.pop();
        } else if (isOperator(tok)) {
            while (S.length > 0 && isOperator(S[S.length - 1]) && ((leftAssoc(tok) && priority(tok) <= priority(S[S.length - 1])) || (!leftAssoc(tok) && priority(tok) < priority(S[S.length - 1])))) O.push(S.pop());
            S.push(tok);
        } else {
            O.push(tok);
        }
    }
    while (S.length > 0) {
        if (!isOperator(S[S.length - 1])) return 'Mismatched parenthesis.';
        O.push(S.pop());
    }
    if (O.length == 0) return 'Ошибка ввода.'
    var s = '';
    for (var i = 0; i < O.length; i++) {
        if (i != 0) s += ' ';
        s += O[i];
    }
    return s;
}


def is_operator(ch):
    return ch == '+' or ch == '-' or ch == '*' or ch == '/' or ch == '^'


def leftAssoc(ch):
    return ch != '^'


def priority(ch):
    if ch == '^': return 3
    if ch == '*': return 2
    if ch == '/': return 2
    if ch == '+': return 1
    if ch == '-':  return 1
    return 0


def rightPriority(ch):
    if ch == '+': return 1
    if ch == '-': return 2
    if ch == '*': return 3
    if ch == '/': return 4
    if ch == '^': return 5;
    return 0;


def polska(expression):
    i = 0

    def next_token(i):
        while i < len(expression) and expression[i] == ' ':
            i += 1
            if i == len(expression): return ''
            b = ''
            while i < len(expression) and expression[i] != ' ' \
                    and expression[i] != '(' \
                    and expression[i] != ')' \
                    and not is_operator(expression[i]):
                i += 1
                b += expression[i]
            if b != '': return b
            return expression[i + 1]

    symb = []
    operant = []
    token = next_token(i)
    while token != '':
        if token == '(':
            symb.append(token)
        elif token == ')':
            while len(symb) > 0 and symb[-1] != '(':
                operant.append(symb.pop())
            if len(symb) == 0:
                return 'Error expression'
            symb.pop()
        elif is_operator(token):
            while (len(symb) > 0 and is_operator(symb[-1])
                   and ((leftAssoc(token) and priority(token) <= priority(symb[-1]))
                        or (not leftAssoc(token) and priority(token) < priority(symb[-1])))):
                operant.append(symb.pop())
            symb.append(token)
        else:
            operant.append(token)
        i += 1
        token = next_token(i)

    while len(symb) > 0:
        if not is_operator(symb[-1]): return 'Error expression'
        operant.append(symb.pop())

    if len(operant) == 0: return 'Error expression'

    result = ''
    for ch in range(len(operant) - 1):
        if ch != 0:
            result += ' '
        result += operant[ch]

    return result


def str_to_float(expression):
    if expression.count('(') != expression.count(')'):
        print(f'Error in expression: Not Balance in Brackets')
        return
    # gen(expression.count('('),0,0,"")
    print(polska(expression))
