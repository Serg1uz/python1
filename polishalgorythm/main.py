input = ['21+((a+ab)*2-(ccc/1))^2',
         'a=2',
         'ab=3',
         'ccc=4'
         ]



def get_expression(arr):
    return arr[0]


def get_variables(arr):
    result = {line.split('=')[0]: line.split('=')[1] for line in arr[1:]}
    return result


def sort_variables(arr):
    result = {}
    for k in sorted(arr, key=len, reverse=True):
        result[k] = arr[k]
    return result


def replace_expession(expression, arr):
    for key, value in arr.items():
        expression = expression.replace(key, value)
    return expression


def main():
    a = 2
    expression = get_expression(input)
    print(expression)
    variables = get_variables(input)
    print(variables)
    variables = sort_variables(variables)
    print(variables)
    expression = replace_expession(expression, variables)
    print(expression)
    # print(f'{expression}')
    # print(eval(expression))
    # a = 2+(2+3)*2-(4/1)**2
    # print(a)
    # str_to_float(expression)


if __name__ == "__main__":
    main()
