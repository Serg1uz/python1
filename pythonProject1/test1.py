JIRA_STATUS_OPEN = ['open', 'not actual']
JIRA_STATUS_NEW = ['to do']

def check(s: str):
    s = s.lower()

    try:
        result = {
            s in JIRA_STATUS_OPEN: 'open',
            s in JIRA_STATUS_NEW: 'new',
        }[True]
    except KeyError:
        result = 'bla bla'
    return result

s = 'To Do'
s1 = s.lower()
print(f'{s1 in JIRA_STATUS_NEW} - {check(s)}')

s = 'Open'
s1 = s.lower()
print(f'{s1 in JIRA_STATUS_NEW} - {check(s)}')

s = 'Bla'
s1 = s.lower()
print(f'{s1 in JIRA_STATUS_NEW} - {check(s)}')
