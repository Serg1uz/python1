from django.urls import path, include
from rest_framework import routers

from .views import OurLogViewSet

router = routers.DefaultRouter()
router.register(r'ourlog', OurLogViewSet, basename='ourlog')


urlpatterns = [
    path('', include(router.urls)),
]