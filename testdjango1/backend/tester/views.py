from rest_framework import viewsets, status
from rest_framework.response import Response
from .models import Ourlog
from .serializer import OurLogSeriazer


class OurLogViewSet(viewsets.GenericViewSet):
    queryset = Ourlog.objects.all()
    serializer_class = OurLogSeriazer