from django.db import models


class Ourlog(models.Model):
    OUR_STATUS = [
        ('open', 'Open'),
        ('001_new', '001 New'),
        ('002_in_work', '002 In Work'),
        ('003_testing', '003 Testing'),
        ('004_fix_issue', '004 Fix Issue'),
        ('005_core_review', '005 Core Review'),
        ('007_stage_done', '007 Stage Done'),
        ('008_done', '008 Done'),
        ('009_hold', '009 Hold'),
        ('010_discussion', '010 Discussion'),
    ]

    # JIRA_STATUS = (
    #     ('not_actual', 'Not Actual'),
    # )

    our_status = models.CharField('Status', max_length=20, choices=OUR_STATUS, default='open')
    estimated_date = models.DateField ('Estimated Date', blank=True, null=True)
    start_development = models.DateField ('Start Development Date', blank=True, null=True)
    time_development = models.TimeField('Time Development', blank=True, null=True)
